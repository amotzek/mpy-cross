FROM amotzek/behave

RUN apt-get update \
 && apt-get install -y git \
 && apt-get clean

ADD micropython/mpy-cross/mpy-cross /usr/local/bin/mpy-cross

RUN chmod +x /usr/local/bin/mpy-cross
