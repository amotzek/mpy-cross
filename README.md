# mpy-cross

Dieses Projekt baut das Docker Image amotzek/mpy-cross.

Dabei wird zuerst mit git clone --branch $TAG https://github.com/micropython/micropython.git 
das gewünschte Tag des Projekts micropython/micropython aus GitHub ausgecheckt. Dann wird
make im Verzeichnis mpy-cross ausgeführt, um den MicroPython Cross Compiler zu bauen. Der 
gebaute Compiler wird schließlich in ein Docker Image verpackt und unter 
https://hub.docker.com/r/amotzek/mpy-cross veröffentlicht.
